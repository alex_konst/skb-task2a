const express = require('express');
const app = express();

app.options('/', function (req, res) {
    res.set('Access-Control-Allow-Origin', '*');
    res.set('Access-Control-Allow-Headers', 'Content-Type');
    res.status(200).send();
});

app.get('/', function (req, res) {
    const query = req.query;
    const keysAvailable = ['a', 'b'];
    const sum = keysAvailable.reduce((sum, key) => {
        let value = query[key] && !isNaN(parseInt(query[key])) ? parseInt(query[key]) : 0;

        return sum + value;
    }, 0);

    res.set('Access-Control-Allow-Origin', '*');
    res.send(String(sum));
});

app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});